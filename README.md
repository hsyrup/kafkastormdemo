
# DATA ECOSYSTEM
## Stream Computing Platform using Apache Storm, Kafka and Hbase 

>Nov.-Dec. 2016
>School of computer science and engineering, Fudan University                           
>Supervisor: A.P. Peng Wang

* Built a pseudo distributed stream computing platform. Get passages from Kafka topic, deal with them on Storm, and finally store the result into Hbase.
* Developed a wordcount program running on the system mentioned above, using Java API provided by Apache software.

