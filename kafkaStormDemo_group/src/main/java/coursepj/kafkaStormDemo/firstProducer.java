package coursepj.kafkaStormDemo;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

public class firstProducer
{
	Properties props;
	Producer<String, String> producer;
	int mark = 0;
	
	public firstProducer()
	{
		Properties props = new Properties();
		props.put("bootstrap.servers", "hadoop1:9092");
		props.put("acks", "all");
		props.put("retries", 0);
		props.put("batch.size", 16384);
		props.put("linger.ms", 1);
		props.put("buffer.memory", 33554432);
		props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		this.props = props;
		this.producer = new KafkaProducer<>(props);
	}
	
	public void getsimpleMessage(String topic, String filename){
        File file = new File(filename);
        File[] list=file.listFiles();

        for(int N=0;N<list.length;N++){
            BufferedReader reader = null;
            try {
                reader = new BufferedReader(new FileReader(list[N].getAbsoluteFile()));//??
                String line = null;
                while ((line = reader.readLine()) != null) {
                    producer.send(new ProducerRecord<String, String>(topic, line));
                    mark++;
                    if (mark == 100000){mark = 0;}
                    if (mark == 0){
                    	System.out.println(line.toString());
                    }
                }
                }catch (IOException e) {
                    e.printStackTrace();
               }
        }
        System.out.println("producer.send finished");
    }
	
	public void simpleMessage(String topic)
    {
		producer.send(new ProducerRecord<String, String>(topic, "apple apple banana apple, aprico, papaya peach avocado avocado."));
//		producer.send(new ProducerRecord<String, String>(topic,  "The speaker conveys doubt about all 3 points mentioned by the author, who consider new stricter regulations for handling and storing coal ash unnecessary."));
//		producer.send(new ProducerRecord<String, String>(topic,  "Firstly, the speaker points out that, although all newly build disposal ponds or landfills are requested to use liner, as the author mentions"));
//		producer.send(new ProducerRecord<String, String>(topic,  "mentions in the reading passage, those old ponds and landfills are not requested to use liner, which means they would do harm to environment."));
		producer.close();
		System.out.println("producer.send finished");
    }
	
	public void close()
	{
		this.producer.close();
	}

	public static void main(String args[]){
		firstProducer fpr=new firstProducer();
//		fpr.getsimpleMessage(args[0],args[1]);
		fpr.simpleMessage(args[0]);
		fpr.close();
	}
}	


