package coursepj.kafkaStormDemo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.IRichBolt;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

public class spliterBolt implements IRichBolt {

	private static final long serialVersionUID = -2815991779975708080L;
	private OutputCollector collector; 
	int mark = 0;
//	List<Object> temp= new ArrayList<Object>();
	
	@Override
	public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
		this.collector = collector; 
		System.out.println("sb prepare");
	}

	@Override
	public void execute(Tuple input) {
		System.out.println("sb execute");
		String sentence = input.getString(0);
		sentence = sentence.replaceAll("[^a-zA-Z]", " "); //锟斤拷每一锟叫斤拷锟叫达拷锟斤拷锟窖凤拷锟斤拷母锟芥换锟缴空革拷
		sentence = sentence.replaceAll("	", " ");
		String[] words = sentence.split(" ");  
		for (String word : words) 
		{  
			word = word.trim(); 
			word = word.toLowerCase();
			if (!word.isEmpty()) 
			{  
/*				temp.add(word);
				mark++;
				if (mark==10000){
					collector.emit(temp);
					mark = 0;
					temp = null;
				}	*/	
				collector.emit(new Values(word));
			}  
		}  
		collector.ack(input);
//		System.out.println("spliting finished");
	}

	@Override
	public void cleanup() {	
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("word"));  
		System.out.println("sb declare");
	}

	@Override
	public Map<String, Object> getComponentConfiguration() {
		return null;
	}

}
