package coursepj.kafkaStormDemo;

import java.util.ArrayList;
import java.util.Arrays;

import org.apache.storm.Config;
import org.apache.storm.kafka.BrokerHosts;
import org.apache.storm.kafka.KafkaSpout;
import org.apache.storm.kafka.SpoutConfig;
import org.apache.storm.kafka.StringScheme;
import org.apache.storm.kafka.ZkHosts;
import org.apache.storm.spout.SchemeAsMultiScheme;
import org.apache.storm.topology.TopologyBuilder;
import org.apache.storm.tuple.Fields;
import org.apache.storm.StormSubmitter;
import org.apache.storm.generated.AlreadyAliveException;
import org.apache.storm.generated.AuthorizationException;
import org.apache.storm.generated.InvalidTopologyException;

public class firstTopology {

	public static void main(String args[]){

		BrokerHosts brokerHosts = new ZkHosts("hadoop1:2181");
		String topic = args[0];
		String zkRoot = args[1];
		String spoutId = args[2];
		SpoutConfig kafkaConfig = new SpoutConfig(brokerHosts,topic,zkRoot,spoutId);
		
		kafkaConfig.scheme = new SchemeAsMultiScheme(new StringScheme());
		kafkaConfig.zkServers = Arrays.asList(new String[] {"hadoop1"}); 
		kafkaConfig.zkPort = 2181; 

//		kafkaConfig.forceFromStart=true;
		
		KafkaSpout spout = new KafkaSpout(kafkaConfig);
		
		TopologyBuilder builder = new TopologyBuilder();  
		builder.setSpout("spout", spout, 3);  
		builder.setBolt("spliterbolt", new spliterBolt(), 3).shuffleGrouping("spout").setNumTasks(3); 
//		builder.setBolt("countbolt", new countBolt(), 3).fieldsGrouping("spliterbolt",new Fields("word")).setNumTasks(3);
		builder.setBolt("countbolt", new countBolt(), 3).shuffleGrouping("spliterbolt").setNumTasks(3);
		builder.setBolt("hbasebolt", new hbaseBolt(), 3).shuffleGrouping("countbolt").setNumTasks(3);
		
		Config conf = new Config();
		conf.put(Config.NIMBUS_SEEDS, Arrays.asList("hadoop1"));
//		conf.setDebug(true);  
		conf.setNumWorkers(2); 
		
//		conf.setMaxTaskParallelism(3);  
/*		StormSubmitter cluster = new StormSubmitter();*/
		try {
			StormSubmitter.submitTopologyWithProgressBar("WritingSample_fruit1", conf, builder.createTopology());
		} catch (AlreadyAliveException | InvalidTopologyException | AuthorizationException e) {
			e.printStackTrace();
		}
	}
}
