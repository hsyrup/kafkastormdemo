package coursepj.kafkaStormDemo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.IRichBolt;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

public class countBolt implements IRichBolt {

	private static final long serialVersionUID = -7042825992549305696L;
	Integer id;  
	String name;  
	Map<String, Integer> counters;  
	OutputCollector collector;
	int emitmark;
	
	@Override
	public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
		System.out.println("cb prepare1");
		this.counters = new HashMap<String, Integer>();
		this.emitmark = 0;
		this.collector = collector;  
		this.name = context.getThisComponentId();  
		this.id = context.getThisTaskId(); 
		System.out.println("cb prepare2");
	}

	@Override
	public void execute(Tuple input) {
		System.out.println("cb execute1");
/*		for (int i=0;i<10000;i++){
			String str = input.getString(i);  
			this.emitmark++;
			if(this.emitmark == 100000){
				this.emitmark = 0;
			}
			if (!counters.containsKey(str)) 
			{  
				counters.put(str, 1);  
			} 
			else 
			{  
				Integer c = counters.get(str) + 1;  
				counters.put(str, c);  
			} 
			if(this.emitmark == 0){
				for (String key : counters.keySet()) {
					List<Tuple> a = new ArrayList<Tuple>();  
					a.add(input);  
					collector.emit(new Values(key+" "+counters.get(key)));
				}
			}
		}*/
		String str = input.getString(0);  
		this.emitmark++;
		if(this.emitmark == 100000){
			this.emitmark = 0;
		}
		if (!counters.containsKey(str)) 
		{  
			counters.put(str, 1);  
		} 
		else 
		{  
			Integer c = counters.get(str) + 1;  
			counters.put(str, c);  
		} 
		if(this.emitmark == 0){
			for (String key : counters.keySet()) {
				List<Tuple> a = new ArrayList<Tuple>();  
				a.add(input);  
				collector.emit(new Values(key+" "+counters.get(key)));
			}
		}
		
		collector.ack(input);
//		System.out.println("counting finished");
	}

	@Override
	public void cleanup() {
		System.out.println("-- Word Counter [" + name + "-" + id + "] --");  
		for (Map.Entry<String, Integer> entry : counters.entrySet())
		{  
			System.out.println(entry.getKey() + ": " + entry.getValue());  
		}  
		counters.clear();  
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		System.out.println("cb declare1");
		declarer.declare(new Fields("word"));  
		System.out.println("cb declare2");
	}

	@Override
	public Map<String, Object> getComponentConfiguration() {
		return null;
	}

}
