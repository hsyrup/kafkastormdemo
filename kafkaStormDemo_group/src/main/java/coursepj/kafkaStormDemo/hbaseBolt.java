package coursepj.kafkaStormDemo;

import java.io.IOException;
import java.util.Map;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.IRichBolt;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;

public class hbaseBolt implements IRichBolt{

	private static final long serialVersionUID = -3740828381189065818L;
	private Connection connection;
    private Table table;
    OutputCollector collector; 
	
    public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) 
    {
    	Configuration config = HBaseConfiguration.create();
    	config.set("hbase.zookeeper.quorum", "hadoop1");
    	this.collector = collector;
        try {
            connection = ConnectionFactory.createConnection(config);
//            table = connection.getTable(TableName.valueOf("HPWordCount"));
            table = connection.getTable(TableName.valueOf("FC"));
        } catch (IOException e) {
            System.out.println("fail to reach table WordCount");
        }
    }
    
    @Override
	public void execute(Tuple input) 
    {
    	String line = input.getString(0);
    	String [] words = line.split(" ");
    	String word = words[0];
        String count = words[1];
        try {
            Put put = new Put(Bytes.toBytes(word));
            put.addColumn(Bytes.toBytes("cf"), Bytes.toBytes("words"), Bytes.toBytes(word));
            put.addColumn(Bytes.toBytes("cf"), Bytes.toBytes("counts"), Bytes.toBytes(count));
            table.put(put);
        } catch (IOException e) {
        	System.out.println("ERROR in writing table");
        }
        collector.ack(input);

	}
    
    public void cleanup() {
    	System.out.println("hbb cleanup1");
        try {
            if(table != null) table.close();
            System.out.println("hbb cleanup2");
        } catch (Exception e){
        	System.out.println("ERROR in close table");
        } finally {
            try {
                connection.close();
            } catch (IOException e) {
            	System.out.println("ERROR in close connection");
            }
        }
        System.out.println("hbb cleanup3");
    }

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		System.out.println("hbb declare1");
		declarer.declare(new Fields("word"));  
		System.out.println("hbb declare2");
	}

	@Override
	public Map<String, Object> getComponentConfiguration() {
		// TODO Auto-generated method stub
		return null;
	}

}
